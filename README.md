# queens
Generates all possible solutions for a given board size to the eight queens problem (https://en.wikipedia.org/wiki/Eight_queens_puzzle)

## Installation
Install Crystal (https://crystal-lang.org/docs/installation/)  
Compile with: crystal build src/queens.cr --release

## Usage
queens [arguments]  
    -m MIN, --min=MIN                Sets minimum board size [1-20]. Defaults to 1.  
    -M MAX, --max=MAX                Sets maximum board size [1-20]. Defaults to 8.  
    -s SIZE, --size=SIZE             Runs for selected board size [1-20].  
    -p, --print                      Prints additional info.  
    -bm, --benchmark                 Runs benchmark.  
    -ips, --ips                      Calculates [IPS](https://crystal-lang.org/api/0.25.1/Benchmark/IPS.html) as benchmark, overrides print, export, and benchmark settings.  
    -h, --help                       Show this help  

## Development
1st attempt: Loop over entire board, checking if each position can have a queen if so add one, compute new restrictions, and repeat until @size queens added (8x8: 8.5min)  
2nd attempt: Loop over entire board, checking if each position can have a queen if so add one, compute new restrictions, and repeat starting at next position @size queens added (8x8: 0.95sec)  
3rd attempt: Check current row, checking if each position can have a queen if so add one, compute new restrictions, and repeat for next row until @size queens added (8x8: 3ms)  
4th attempt: Swapped out Tile class for Enum, dup board before recursive call so I can quickly set to previous state without computing restrictions again and can remove loop for clearing previously blocked tiles (8x8: 1.9ms)  
5th attempt: Only calculate restrictions for newly added queen now that we can easily restore previous state (8x8 600µs)
6th attempt: Only calculate restrictions on future cells to visit, instantiate restriction variables outside loop to help GC, remove export options, and only store count of found solutions (8x8: 246µs)

## Contributors

- [CJ Fravel](https://gitlab.com/cjfravel) CJ Fravel - creator, maintainer
