require "benchmark"
require "option_parser"
require "./queens/*"

def process_args
  min_size = 1_u8
  max_size = 8_u8
  print_results = false
  benchmark = false
  ips = false
  help = false
  OptionParser.parse! do |parser|
    parser.banner = "Usage: queens [arguments]"
    parser.on("-m MIN", "--min=MIN", "Sets minimum board size [1-20]. Defaults to 1.") { |arg| min_size = arg.to_u8 }
    parser.on("-M MAX", "--max=MAX", "Sets maximum board size [1-20]. Defaults to 8.") { |arg| max_size = arg.to_u8 }
    parser.on("-s SIZE", "--size=SIZE", "Runs for selected board size [1-20].") { |arg| min_size = max_size = arg.to_u8 }
    parser.on("-p", "--print", "Prints additional info.") { print_results = true }
    parser.on("-bm", "--benchmark", "Runs benchmark.") { benchmark = true }
    parser.on("-ips", "--ips", "Calculates IPS as benchmark, overrides print and benchmark settings.") { ips = true }
    parser.on("-h", "--help", "Show this help.") {
      puts parser
      help = true
    }
    parser.invalid_option {
      puts parser
      help = true
    }
    parser.missing_option {
      puts parser
      help = true
    }
  end
  min_size = max_size if min_size > max_size
  max_size = 20_u8 if max_size > 20
  return min_size, max_size, print_results, benchmark, ips, help
end

def run(min_size, max_size, print_results, benchmark, ips)
  if (ips)
    Benchmark.ips { |x|
      (min_size..max_size).each { |size|
        x.report("Board Size: #{size}") {
          board = Queens::Board.new size
          board.process_queens
        }
      }
    }
  elsif (benchmark)
    Benchmark.bm { |x|
      (min_size..max_size).each { |size|
        x.report("Board Size: #{size}") {
          board = Queens::Board.new size
          puts "" if print_results
          board.process_queens
          board.print_results if print_results
        }
      }
    }
  else
    (min_size..max_size).each { |size|
      board = Queens::Board.new size
      puts "Board Size: #{size}" if print_results
      board.process_queens
      board.print_results if print_results
    }
  end
end

min_size, max_size, print_results, benchmark, ips, help = process_args
run(min_size, max_size, print_results, benchmark, ips) if !help
