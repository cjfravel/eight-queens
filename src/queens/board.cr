module Queens
  class Board
    def initialize(size : UInt8)
      @size = size
      @board = Array(Tile).new
      (@size.to_u16*@size).times {
        @board << Tile::Open
      }
      @positions_tried = 0_u64
      @solution_cnt = 0
    end

    def print_results
      puts "Expected Solutions: #{SOLUTION_COUNTS[@size]}"
      puts "Found Solutions: #{@solution_cnt}"
      puts "Max Board Configurations: #{@size.to_u64 ** 2 ** @size}"
      puts "Positions Tried: #{@positions_tried}"
    end

    def process_queens
      process_queens @size, 0
    end

    private def process_queens(n, row)
      if (n == 0)
        @solution_cnt += 1
      else
        get_row_range(row).each { |i|
          if (@board[i] == Tile::Open)
            prev_board = @board.clone
            @board[i] = Tile::Queen
            compute_restrictions i
            @positions_tried += 1
            process_queens(n - 1, row + 1)
            @board = prev_board
          end
        }
      end
    end

    private def compute_restrictions(i)
      if @board[i] == Tile::Queen
        row, col = i.divmod(@size)
        r = row + 1
        c = 1
        row_start = (r*@size)
        row_end = row_start + @size
        column_blocked = row_start + col
        bottom_left_diag = column_blocked - c
        bottom_right_diag = column_blocked + c

        while r < @size
          block column_blocked
          block bottom_left_diag if bottom_left_diag >= row_start
          block bottom_right_diag if bottom_right_diag < row_end

          r += 1
          c += 1
          row_start += @size
          row_end += @size
          column_blocked += @size
          bottom_left_diag = column_blocked - c
          bottom_right_diag = column_blocked + c
        end
      end
    end

    private def block(i)
      @board[i] = Tile::Blocked if @board[i] == Tile::Open
    end

    private def get_row_range(row)
      return ((row * @size)...((row + 1) * @size))
    end
  end
end
