module Queens
  enum Tile : UInt8
    Queen
    Open
    Blocked
  end
end
